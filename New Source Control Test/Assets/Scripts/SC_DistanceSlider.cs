using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SC_DistanceSlider : MonoBehaviour
{
    public GameObject gameState;
    public GameObject room;
    public GameObject item;
    public GameObject obstacle;

    private SC_GameState gameScript;
    // Start is called before the first frame update
    void Start()
    {
        gameScript = (SC_GameState)gameState.GetComponent(typeof(SC_GameState));
        GameObject newRoom;
        RectTransform rt;
        RectTransform roomRT = room.GetComponent<RectTransform>();
        Vector3 pos;

        for (int i = 0; i < gameScript.totalRooms; i++)
        {
            newRoom = Instantiate(room , transform.position, transform.rotation);
            newRoom.transform.parent = gameObject.transform.Find("Room Area");
            rt = newRoom.GetComponent<RectTransform>();
            pos = new Vector3((roomRT.rect.width/2) - 300 + (i * 600 / gameScript.totalRooms), 0, 0);
            rt.localPosition = pos;
            if (i % 3 != 1) newRoom = Instantiate(obstacle, transform.position, transform.rotation);
            else newRoom = Instantiate(item, transform.position, transform.rotation);
            newRoom.transform.parent = gameObject.transform.Find("Obs and Item Area");
            rt = newRoom.GetComponent<RectTransform>();
            pos = new Vector3(-300 + (roomRT.rect.width - (roomRT.rect.width / (1.5f*gameScript.roomLength))) +(i * 600 / gameScript.totalRooms), 0, 0);
            rt.localPosition = pos;
            rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, -10, rt.rect.height);
            rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, -10, rt.rect.width);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}

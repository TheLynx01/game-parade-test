using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SC_DoorBehaviour : MonoBehaviour
{

    public Collider DoorCollider;
    public bool IsOn = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DoorTrigger(Collider DoorCollider)
    {
        IsOn = true;
    }
}

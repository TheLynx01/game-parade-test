using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SC_GameState : MonoBehaviour
{
    public GameObject itemRoom;
    public GameObject obstacleRoom;
    public GameObject victoryRoom;
    public GameObject player;
    public GameObject[] items;
    public GameObject[] obstacles;
    public GameObject[] roomProps;
    public Slider distanceSlider;
    public int roomLength = 8;
    public int totalRooms = 15;
    public int roomsSpawningAtOnce = 3;

    private int roomIndex = 0;
    private int currentRoomIndex = 0;
    public AudioSource audioSource;
    private GameObject[] rooms;
    private bool[] usedItems;
    // Start is called before the first frame update
    void Start()
    {
        rooms = new GameObject[totalRooms];
        usedItems = new bool[items.Length];
        for (int i = 0; i < usedItems.Length; i++) usedItems[i] = false;
        roomIndex = 0;
        currentRoomIndex = -3;
        SpawnRoom(new Vector3(0, 0, -23));
        currentRoomIndex = 0;
        SC_Room roomScript = (SC_Room)rooms[0].GetComponent(typeof(SC_Room));
        roomScript.OpenDoor();
        audioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        distanceSlider.value = (player.transform.position.z / (roomLength * totalRooms));
    }

    public void SetItemUsed(int index)
    {
        usedItems[index] = true;
    }

    public void SetItemFree(int index)
    {
        usedItems[index] = false;
    }

    public bool GetItemUsed(int index)
    {
        return usedItems[index];
    }

    public void SpawnRoom(Vector3 positionOfRoom)
    {
        if (roomIndex < totalRooms)
        {
            if (currentRoomIndex == roomIndex - roomsSpawningAtOnce)
            {
                for (int i = 0; i < roomsSpawningAtOnce; i++)
                {
                    if (i == 1) rooms[roomIndex] = Instantiate(itemRoom, positionOfRoom + new Vector3(0, 0, (roomLength * roomsSpawningAtOnce) + (roomLength * i)), Quaternion.identity);
                    else rooms[roomIndex] = Instantiate(obstacleRoom, positionOfRoom + new Vector3(0, 0, (roomLength * roomsSpawningAtOnce) + (roomLength * i)), Quaternion.identity);
                    roomIndex++;
                }
            }
        }
        else if (currentRoomIndex == totalRooms- roomsSpawningAtOnce)
        {
            Instantiate(victoryRoom, positionOfRoom + new Vector3(0, 0, (roomLength * roomsSpawningAtOnce)), Quaternion.identity);
        }
        if (currentRoomIndex >= 0 && currentRoomIndex < totalRooms - 1)
        {
            SC_Room roomScript = (SC_Room)rooms[currentRoomIndex + 1].GetComponent(typeof(SC_Room));
            roomScript.OpenDoor();
        }
        currentRoomIndex++;
    }

}

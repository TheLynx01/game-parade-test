using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SC_UIRotation : MonoBehaviour
{
    public Vector2 turn;
    public float sensitivity = 0.05f;

    public float limit = 0.75f;


    private void Awake()
    {
        //This locks the pointer to the current window size
        //Cursor.lockState = CursorLockMode.Confined;
    }

    public void CheckLimits()
    {
        if (turn.x > limit)
        {
            turn.x = limit;
        }
        else if (turn.x < -limit)
        {
            turn.x = -limit;
        }

        if (turn.y > limit)
        {
            turn.y = limit;
        }
        else if (turn.y < -limit)
        {
            turn.y = -limit;
        }
    }

    void Update()
    {

        turn.x += Input.GetAxis("Mouse X") * sensitivity;
        turn.y += Input.GetAxis("Mouse Y") * sensitivity;

        CheckLimits();

        transform.localRotation = Quaternion.Euler(-turn.y, turn.x, 0);

    }
}

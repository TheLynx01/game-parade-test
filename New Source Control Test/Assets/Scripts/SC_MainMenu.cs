using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SC_MainMenu : MonoBehaviour
{
    public GameObject MainMenu;
    public GameObject Canvas;
    public GameObject ScoreMenu;
    public Camera menuCamera;
    public Text[] Scores;

    public AudioSource musicAudio;
    public AudioClip glassAudio;
    public AudioClip musicIntro;
    public AudioClip musicLoop;
    public Animator FrogAnimator;

    public Image FadeBlack;

    public GameObject SkipButton;

    public int fieldOfViewTarget;
    public Quaternion targetRotation;

    private bool bFade = false;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        SkipButton.SetActive(false);
        GoBackButton();
        Canvas.SetActive(true);
        FadeBlack.color = new Color(0, 0, 0, 0);
        bFade = false;
        for (int i = 0; i < Scores.Length; i++)
        {
            Scores[i].text = PlayerPrefs.GetInt("HighScore" + i.ToString(), 0).ToString();
        }

        musicAudio.loop = true;
        StartCoroutine(playMusic());
    }

    void Update()
    {
        if (Canvas.activeSelf == false)
        {
            if (menuCamera.fieldOfView > fieldOfViewTarget) menuCamera.fieldOfView -= .05f;
            if (menuCamera.transform.rotation != targetRotation) menuCamera.transform.rotation = Quaternion.Lerp(menuCamera.transform.rotation, targetRotation, Time.deltaTime);
            if (menuCamera.fieldOfView <= fieldOfViewTarget && menuCamera.transform.rotation == targetRotation)
            {
                FrogAnimator.SetTrigger("OpenEyes");
                Invoke("FadeToBlack", 1.0f);
            }
        }
        if (bFade)
        {
            FadeBlack.color = new Color(0, 0, 0, FadeBlack.color.a + .008f);
            if (FadeBlack.color.a > 0.95)
            {
                bFade = false;
                StopCoroutine(playMusic());
                musicAudio.Stop();
                musicAudio.PlayOneShot(glassAudio);
                Invoke("LoadLevel", glassAudio.length);
            }
        }
    }


    private void FadeToBlack()
    {
        bFade = true;
    }

    IEnumerator playMusic()
    {
        musicAudio.clip = musicIntro;
        musicAudio.Play();
        yield return new WaitForSeconds(musicAudio.clip.length);
        musicAudio.clip = musicLoop;
        musicAudio.Play();
    }


    public void PlayNowButton()
    {
        Canvas.SetActive(false);
        SkipButton.SetActive(true);
    }

    public void LoadLevel()
    {
        SceneManager.LoadScene("GameLevel");
    }

    public void GoBackButton()
    {
        // Show Main Menu
        MainMenu.SetActive(true);
        ScoreMenu.SetActive(false);
    }

    public void ScoreButton()
    {
        MainMenu.SetActive(false);
        ScoreMenu.SetActive(true);
    }

    public void ExitButton()
    {
        // Quit Game
        Application.Quit();
    }
}



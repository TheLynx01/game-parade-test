using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SC_Room : MonoBehaviour
{
    private GameObject gameState;
    private GameObject[] itemArray;
    private int[] chosenItems;
    private bool bGap = false;
    public int roomIndex = 0;
    public float laneDiff = 0.9f;
    public Animator DoorAnimator;
    // Start is called before the first frame update
    void Start()
    {
        gameState = GameObject.Find("GameState");
        SC_GameState gameScript = (SC_GameState)gameState.GetComponent(typeof(SC_GameState));
        bGap = false;
        if (roomIndex == 0)
        {
            itemArray = new GameObject[3];
            chosenItems = new int[3];
            chosenItems[0] = -1;
            chosenItems[1] = -1;
            for (int i = 0; i < 3; i++)
            {
                int randomObject = -1;
                do
                {
                    randomObject = Random.Range(0, gameScript.items.Length);
                }
                while (gameScript.GetItemUsed(randomObject) == true || randomObject == chosenItems[0] || randomObject == chosenItems[1]);
                chosenItems[i] = randomObject;
                gameScript.SetItemUsed(randomObject);
                itemArray[i] = Instantiate(gameScript.items[randomObject], new Vector3(-laneDiff + (i * laneDiff), 0, transform.position.z + 7.0f), Quaternion.identity);
            }
        }
        else if (roomIndex == 1)
        {
            for (int i = 0; i < 3; i++)
            {
                if (!bGap)
                {
                    if (Random.Range(0, 12) == 0)
                    {
                        bGap = true;
                        continue;
                    }
                }
                int randomObject = Random.Range(0, gameScript.obstacles.Length);
                Instantiate(gameScript.obstacles[randomObject], new Vector3(-laneDiff + (i * laneDiff), 0, transform.position.z + 7.0f), Quaternion.identity);
            }
        }
        Instantiate(gameScript.roomProps[Random.Range(0, gameScript.roomProps.Length)], transform.position, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        if (roomIndex == 0)
        {
            if (itemArray[1] == null)
            {
                Destroy(itemArray[2]);
                Destroy(itemArray[0]);
                SC_GameState gameScript = (SC_GameState)gameState.GetComponent(typeof(SC_GameState));
                gameScript.SetItemFree(chosenItems[2]);
                gameScript.SetItemFree(chosenItems[0]);
                roomIndex = -1;
            }
            else if (itemArray[2] == null)
            {
                Destroy(itemArray[1]);
                Destroy(itemArray[0]);
                SC_GameState gameScript = (SC_GameState)gameState.GetComponent(typeof(SC_GameState));
                gameScript.SetItemFree(chosenItems[1]);
                gameScript.SetItemFree(chosenItems[0]);
                roomIndex = -1;
            }
            else if (itemArray[0] == null)
            {
                Destroy(itemArray[2]);
                Destroy(itemArray[1]);
                SC_GameState gameScript = (SC_GameState)gameState.GetComponent(typeof(SC_GameState));
                gameScript.SetItemFree(chosenItems[2]);
                gameScript.SetItemFree(chosenItems[1]);
                roomIndex = -1;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && roomIndex < 2)
        {
            SC_GameState gameScript = (SC_GameState)gameState.GetComponent(typeof(SC_GameState));
            gameScript.SpawnRoom(transform.position);
        }
    }

    public void OpenDoor()
    {
        DoorAnimator.SetTrigger("OpenDoor");
    }
}

using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine.UI;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public class SC_PlayerController : MonoBehaviour
{
    public float laneChangeSpeed = 1;
    public float speed = 2;
    public float laneDiff = 0.9f;
    public float staggerLength = .1f;
    public GameObject model;
    public GameObject[] itemUIArray;
    public AudioSource[] SFXArray;
    private GameObject GameState;
    public GameObject PauseMenu;
    public GameObject VictoryMenu;
    public GameObject GameOverMenu;
    public GameObject Canvas;
    public Text scoreText;
    public Text finalScoreText;
    public Animator PlayerAnimator;

    private GameObject[] inventoryArray = new GameObject[6];
    private int inventoryFillIndex = 0;
    private bool bSlide = false;
    private bool bJump = false;
    private bool bBreak = false;
    private bool bTeleport = false;
    private bool bGameOver = false;
    private bool bVictory = false;
    private bool bStagger = false;
    private bool bInAbilityZone = false;
    private int lane = 1;
    private int score = 1;
    private int fullCount = 0;
    private Vector3 laneDifference;
    private Vector3 lanePosition;
    // Start is called before the first frame update
    void Start()
    {
        GameState = GameObject.Find("GameState");
        laneDifference = new Vector3(laneDiff, 0.0f, 0.0f);
        lanePosition = transform.position;
        PauseMenu.SetActive(false);
        VictoryMenu.SetActive(false);
        GameOverMenu.SetActive(false);
        bSlide = false;
        bJump = false;
        bBreak = false;
        bGameOver = false;
        bVictory = false;
        inventoryFillIndex = 0;
        SFXArray[0].Play();
        SFXArray[1].Play();
        for (inventoryFillIndex = 0; inventoryFillIndex < 3; inventoryFillIndex++)
        {
            inventoryArray[inventoryFillIndex] = Instantiate(itemUIArray[inventoryFillIndex + 10], Canvas.transform.position, Canvas.transform.rotation);
            inventoryArray[inventoryFillIndex].transform.parent = Canvas.transform.Find("Ability Area");
            RectTransform rt = inventoryArray[inventoryFillIndex].GetComponent<RectTransform>();
            Vector3 pos = new Vector3(-250 + (inventoryFillIndex * 100), 0, 0);
            rt.localPosition = pos;
            fullCount++;
        }
    }

    void Update()
    {
        if (!PauseMenu.activeSelf && !VictoryMenu.activeSelf && !GameOverMenu.activeSelf) Time.timeScale = 1;
        Move();
        Ability();
        if (Input.GetKeyDown(KeyCode.Escape) && !bGameOver && !bVictory) TogglePause();
        if (!bGameOver && !bVictory) UpdateScore();
    }

    public void TogglePause()
    {
        if (Time.timeScale == 1)
        {
            SFXArray[0].Stop();
            SFXArray[1].Stop();
            Time.timeScale = 0;
            PauseMenu.SetActive(true);
        }
        else
        {
            SFXArray[0].Play();
            SFXArray[1].Play();
            Time.timeScale = 1;
            PauseMenu.SetActive(false);
        }
    }

    private void Debug()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            GameOver();
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            Victory();
        }
        else if (Input.GetKeyDown(KeyCode.Minus))
        {
            score--;
        }
        else if (Input.GetKeyDown(KeyCode.Equals))
        {
            score++;
        }
    }

    private void UpdateScore()
    {
        scoreText.text = "Score: " + score.ToString();
        if (score < 0) GameOver();
    }

    private void GameOver()
    {
        SFXArray[0].Stop();
        SFXArray[1].Stop();
        Time.timeScale = 0;
        bGameOver = true;
        GameOverMenu.SetActive(true);
    }

    private void Victory()
    {
        SFXArray[0].Stop();
        SFXArray[1].Stop();
        Time.timeScale = 0;
        bVictory = true;
        scoreText.text = "";
        finalScoreText.text = "Final Score: " + score.ToString();
        for (int i = 4; i > -1; i--)
        {
            int highScore = PlayerPrefs.GetInt("HighScore"+i.ToString(), 0);
            if (highScore < score)
            {
                continue;
            }
            else
            {
                if (i < 4)
                {
                    PlayerPrefs.SetInt("HighScore" + i.ToString(), score);
                    PlayerPrefs.Save();
                }
                break;
            }
        }
        VictoryMenu.SetActive(true);
    }

    private void Move()
    {
        if (!bStagger)
        {
            transform.position += transform.forward * Time.deltaTime * speed;
            lanePosition += transform.forward * Time.deltaTime * speed;
            if (lanePosition == transform.position && !bSlide && !bBreak)
            {
                if ((Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)) && lane > 0)
                {
                    lane--;
                    lanePosition = transform.position - laneDifference;
                }
                if ((Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) && lane < 2)
                {
                    lane++;
                    lanePosition = transform.position + laneDifference;
                }
            }
            else
            {
                transform.position = Vector3.Lerp(transform.position, lanePosition, Time.deltaTime * laneChangeSpeed);
                if (Mathf.Abs(transform.position.x - lanePosition.x) < 0.05f) transform.position = lanePosition;
            }
        }
    }

    private void Ability()
    {
        if (bInAbilityZone)
        {
            int inventoryIndex = -1;
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                inventoryIndex = 0;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                inventoryIndex = 1;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                inventoryIndex = 2;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                inventoryIndex = 3;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                inventoryIndex = 4;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                inventoryIndex = 5;
            }
            if (inventoryIndex > -1 && inventoryArray[inventoryIndex] != null)
            {
                if (inventoryArray[inventoryIndex].CompareTag("I_Jump"))
                {
                    SFXArray[0].Stop();
                    SFXArray[1].Stop();
                    SFXArray[3].Play();
                    fullCount--;
                    speed = 1.9f;
                    bInAbilityZone = false;
                    bJump = true;
                    PlayerAnimator.SetTrigger("Jump");
                    Destroy(inventoryArray[inventoryIndex]);
                }
                else if (inventoryArray[inventoryIndex].CompareTag("I_Break"))
                {
                    SFXArray[0].Stop();
                    SFXArray[1].Stop();
                    SFXArray[2].Play();
                    fullCount--;
                    speed = 1.9f;
                    bInAbilityZone = false;
                    bBreak = true;
                    PlayerAnimator.SetTrigger("Tackle");
                    Destroy(inventoryArray[inventoryIndex]);
                    Invoke("EndStagger", .5f);
                }
                else if (inventoryArray[inventoryIndex].CompareTag("I_Slide"))
                {
                    SFXArray[0].Stop();
                    SFXArray[1].Stop();
                    SFXArray[4].Play();
                    fullCount--;
                    speed = 1.9f;
                    bInAbilityZone = false;
                    bSlide = true;
                    PlayerAnimator.SetTrigger("Slide");
                    Destroy(inventoryArray[inventoryIndex]);
                }
                else if (inventoryArray[inventoryIndex].CompareTag("I_Teleport"))
                {
                    bInAbilityZone = false;
                    fullCount--;
                    Destroy(inventoryArray[inventoryIndex]);
                    score++;
                    transform.position += new Vector3(0, 0, 1.5f);
                    lanePosition += new Vector3(0, 0, 1.5f);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        SC_GameState gameScript = (SC_GameState)GameState.GetComponent(typeof(SC_GameState));
        if (other.gameObject.CompareTag("Item"))
        {
            SFXArray[6].Play();
            if (fullCount < 6)
            {
                for (int index = 0; index < gameScript.items.Length; index++)
                {
                    if (other.gameObject.name.Contains(gameScript.items[index].name))
                    {
                        inventoryArray[inventoryFillIndex] = Instantiate(itemUIArray[index], Canvas.transform.position, Canvas.transform.rotation);
                        inventoryArray[inventoryFillIndex].transform.parent = Canvas.transform.Find("Ability Area");
                        RectTransform rt = inventoryArray[inventoryFillIndex].GetComponent<RectTransform>();
                        Vector3 pos = new Vector3(-250 + (inventoryFillIndex * 100), 0, 0);
                        rt.localPosition = pos;
                        do
                        {
                            inventoryFillIndex++;
                            if (inventoryFillIndex > 5) inventoryFillIndex = 0;
                        }
                        while (inventoryArray[inventoryFillIndex] != null);
                        break;
                    }
                }
            }
            Destroy(other.gameObject);
        }
        else if (other.gameObject.CompareTag("ObstacleRoom"))
        {
            bInAbilityZone = true;
        }
        else if (other.gameObject.CompareTag("VictoryRoom"))
        {
            Victory();
        }
        else if (other.gameObject.CompareTag("O_Slide") && bSlide) score++;
        else if (other.gameObject.CompareTag("O_Break") && bBreak)
        {
            score++;
            Destroy(other.gameObject);
            bBreak = false;
        }
        else if (other.gameObject.CompareTag("O_Jump") && bJump) score++;
        else if (!other.gameObject.CompareTag("ItemRoom"))
        {
            SFXArray[0].Stop();
            SFXArray[1].Stop();
            SFXArray[2].Play();
            score--;
            Destroy(other.gameObject);
            bInAbilityZone = false;
            PlayerAnimator.SetTrigger("Stumble");
            speed = 1f;
            Invoke("PlayStagger", .8f);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("ObstacleRoom"))
        {
            bInAbilityZone = false;
        }
        else if (other.gameObject.CompareTag("O_Slide") || other.gameObject.CompareTag("O_Jump") || other.gameObject.CompareTag("O_Break"))
        {
            SFXArray[0].Play();
            SFXArray[1].Play();
            bJump = false;
            bSlide = false;
            speed = 2f;
        }
    }
    private void PlayStagger()
    {
        bStagger = true;
        speed = 2f;
        Invoke("EndStagger", staggerLength);
    }

    private void EndStagger()
    {
        SFXArray[0].Play();
        SFXArray[1].Play();
        bJump = false;
        bSlide = false;
        bBreak = false;
        bStagger = false;
        speed = 2f;
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void Restart()
    {
        SceneManager.LoadScene("GameLevel");
    }
}
